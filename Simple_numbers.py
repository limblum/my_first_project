def search_simple_number(number):
    if number == 2:
        return number
    for i in range(2,number):
        if number % i == 0:
            number += 1
    else:
        return number

#def division_by_simple_number(number,simple_number):
    



number = int(input())
simple_number = 2
simple_number_list = dict()
answer = ''

while number != 1:

    simple_number = search_simple_number(simple_number)
    if number % simple_number == 0:
        number = int(number / simple_number)
        if simple_number not in simple_number_list:
            simple_number_list[simple_number] = 1
        else:
            simple_number_list[simple_number] += 1
        simple_number = 2
    else:
        simple_number += 1

for key in simple_number_list:
    if simple_number_list[key] == 1:
        answer += '('+str(key)+')'
    else:
        answer += '('+str(key)+'**'+str(simple_number_list[key])+')'

print(answer)
    





